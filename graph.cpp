#include <graphics.h>
#include <iostream.h>
#include <conio.h>
#include <alloc.h>
#include <stdlib.h>
#include <fcntl.h>
#include <fstream.h>
// #include <unistd.h>
#include <stdio.h>

static unsigned int colors[16] = {
      0x000,
      0xFA0,
      0x0A0,
      0x0AA,
      0xA00,
      0xA0A,
      0xA50,
      0xAAA,
      0x555,
      0x55F,
      0x5F5,
      0x5FF,
      0xF55,
      0xF5F,
      0xFF5,
      0xFFF
};

struct color3b{
      unsigned char blue;
      unsigned char green;
      unsigned char red;
      color3b()
      {
            blue = 0;
            green = 0;
            red = 0;
      }
      color3b(int colorNum)
      {
            if(colorNum == 20)
                  colorNum = 6;
            else if(colorNum > 7)
            {
                  colorNum-=48;
            }
            *this = colors[colorNum];
      }
      color3b& operator=(unsigned int other)
      {
          for(int i = 0; i < 3; i++)
          {
              char* base_color = ((char*)this) + i;
              *base_color = (other % 16)*17;
              other /= 16;
        }
      return *this;
      }
};

struct ROI{
      unsigned int x1;
      unsigned int y1;
      unsigned int x2;
      unsigned int y2;
      ROI(unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2)
      {
            this->y1 = y1;
            this->y2 = y2;
            this->x1 = x1;
            this->x2 = x2;
      }
};

//define pixelformat of windows bitmap, notice the unusual ordering of colors
struct pixel{
    unsigned char B;
    unsigned char G;
    unsigned char R;
};

//supply an array of pixels[height][width] <- notice that height comes first
int writeBMP(char* filename, const ROI& roi) {
    unsigned int width = roi.x2 - roi.x1;
    unsigned int height = roi.y2 - roi.y1;
    cout << roi.x1  << " " << roi.x2 << " " << width << " " << roi.y1 
    << " " << roi.y2 << " " << height << endl;
    ofstream of(filename, ios::binary); 
    static unsigned char header[54] = {66,77,0,0,0,0,0,0,0,0,54,0,0,0,40,0,0,0,0,0,0,0,0,0,0,0,1,0,24}; //rest is zeroes
    unsigned int pixelBytesPerRow = width*sizeof(color3b);
    unsigned int paddingBytesPerRow = (4-(pixelBytesPerRow%4))%4;
    unsigned int* sizeOfFileEntry = (unsigned int*) &header[2];
    *sizeOfFileEntry = 54 + (pixelBytesPerRow+paddingBytesPerRow)*height;  
    unsigned int* widthEntry = (unsigned int*) &header[18];    
    *widthEntry = width;
    unsigned int* heightEntry = (unsigned int*) &header[22];    
    *heightEntry = height;    
    of.write((const char*)header, 54);
    static unsigned char zeroes[3] = {0,0,0}; //for padding    
    int previous_percent = -1;
    int current_percent = -1;
    color3b* colorRow = new color3b[width];
    for (int row = roi.y2-1; row >= int(roi.y1); row--) {
      for(unsigned int i = roi.x1; i < roi.x2; i++)
      {
            colorRow[i] = color3b(getpixel(i, row));
      }
      current_percent = (int(roi.y2) - row)*100/int(height);
      if(current_percent != previous_percent)
      {
            // cout << current_percent << "% " << int(roi.y2)-1 << " " << row << " " << height << endl;
            previous_percent = current_percent;
      }
      of.write((const char*)colorRow,pixelBytesPerRow);
      of.write((const char*)zeroes,paddingBytesPerRow);
    }
    delete[] colorRow;
    of.close();
    return 0;
}

int main()
{
      int graphdriver=DETECT,graphmode;
      initgraph(&graphdriver,&graphmode,"c:\\tc\\bgi");
      //don't forget to provide right path to bgi driver

      if(graphresult() == 0)
            cout << "Graphics's inited successfully" << endl;
      else
            cout << "Error during graphics init" << endl;

      //draw something
      line(0, 0, 640, 480);
      setcolor(EGA_GREEN);
      cout << "color: " << EGA_GREEN << endl;
      line(640, 0, 0, 480);

      writeBMP("test.bmp", ROI(0, 0, 640, 480));
      cout << "write bmp" << endl;

      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

      getch();
      closegraph();

      //weird stuff~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
      // int size = imagesize(640/2-50, 480/2-50, 640/2+50, 480/2+50);
      // void far * buf = farmalloc(size); 
      // getimage(640/2-50, 480/2-50, 640/2+50, 480/2+50, buf);
      //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

      // cout << size;
      // cout << sizeof(void far *) << endl;

      // getch();
      // initgraph(&graphdriver,&graphmode,"c:\\tc\\bgi");
      // putimage(0,0,buf,COPY_PUT);
      // delete[] buf;
      // getch();
      // closegraph();
      return 0;
}